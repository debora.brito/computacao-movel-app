## Sobre o projeto

Este projeto visa criar uma aplicação para registrar vendas realizadas

## Requisitos
- Node.js 14 (LTS)
- Yarn 1 ou NPM;
- JDK 11 (LTS);
- Android Studio e dependências.

## Como instalar as dependências 

### Com o android studio
https://react-native.rocketseat.dev/android/linux

## Com dispositivo físico
https://react-native.rocketseat.dev/usb/android

## Como rodar esse projeto
$ git clone https://gitlab.com/debora.brito/computacao-movel.git <br>
$ cd computacao <br>
$ npm install <br>
$ cp .env.example .env <br>
$ npm start <br>
$ npx react-native run-android <br>

- Acessar pelo android studio ou dispositivo conectado
