import axios from 'axios'

const api = axios.create({
  baseURL: 'http://192.168.0.126:8000/api',
})

const HttpClient = {
  post: async (url, body, token = '') => {
    let response
    await api
      .post(url, body, {
        headers: {
          Authorization: token,
        },
      })
      .then((data) => {
        response = data
      })
      .catch((error) => {
        response = error.response
      })
    return response
  },
  get: async (url, token) => {
    let response
    await api
      .get(url, {
        headers: {
          Authorization: token,
        },
      })
      .then((data) => {
        response = data
      })
      .catch((error) => {
        response = error.response
      })
    return response
  },
  delete: async (url, token) => {
    let response
    await api
      .delete(url, {
        headers: {
          Authorization: token,
        },
      })
      .then((data) => {
        response = data
      })
      .catch((error) => {
        response = error.response
      })
    return response
  },
  put: async (url, body, token) => {
    let response
    await api
      .put(url, body, {
        headers: {
          Authorization: token,
        },
      })
      .then((data) => {
        response = data
      })
      .catch((error) => {
        response = error.response
      })
    return response
  },
}
export default HttpClient
