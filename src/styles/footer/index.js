import { StyleSheet } from 'react-native'

export const styles = StyleSheet.create({
  FooterCopyright: {
    backgroundColor: '#009624',
    padding: 15,
    bottom: 0,
    left: 0,
    right: 0,
    height: '7%',
    alignItems: 'center',
    position: 'absolute',
    width: '100%',
  },
})
export default styles
