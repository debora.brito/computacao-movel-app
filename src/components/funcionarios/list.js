import React, { useState, useEffect } from 'react'
import {
  View,
  TouchableOpacity,
  Text,
  FlatList,
  ActivityIndicator,
} from 'react-native'
import styles from '../../styles'
import Footer from '../footer'
import Icon from 'react-native-vector-icons/MaterialIcons'
import Icone from 'react-native-vector-icons/FontAwesome'
import CriticalPopup from '../popup'
import HttpClient from '../../services/api'
import AsyncStorage from '@react-native-async-storage/async-storage'

function list({ navigation }) {
  const [showCriticalPopup, setShowCriticalPopup] = useState(false)
  const [data, setData] = useState([])
  const [loader, setLoader] = useState(false)
  useEffect(() => {
    async function loadFunc() {
      try {
        const token = await AsyncStorage.getItem('@ComputacaoApp:token')
        const response = await HttpClient.get('/waiters', token)
        setData(response.data.waiters)
        setLoader(true)
        console.log(response.data.waiters)
      } catch (error) {
        console.log(error)
      }
    }
    loadFunc()
  }, [])
  return (
    <View style={styles.container}>
      <TouchableOpacity
        style={styles.greenButton}
        onPress={() => navigation.navigate('CadastrarFunc')}
      >
        <Text style={styles.buttonText}>Cadastrar Novo Garçom</Text>
      </TouchableOpacity>
      <Text style={styles.headingClient}>Funcionários</Text>
      {!loader && (
        <View style={[styles.loader, styles.horizontal]}>
          <ActivityIndicator size='large' color='#00ff00' />
        </View>
      )}
      <View style={styles.list}>
        <FlatList
          data={data}
          renderItem={({ item }) => (
            <View style={styles.listItem}>
              <Text style={styles.listInfo}>{item.name}</Text>
              <View
                style={{
                  justifyContent: 'space-between',
                  display: 'flex',
                  position: 'relative',
                  flexDirection: 'row',
                  flexWrap: 'wrap',
                  paddingEnd: 10,
                }}
              >
                <TouchableOpacity
                  onPress={() => navigation.navigate('EditFunc')}
                  style={{ marginEnd: 10 }}
                >
                  <Icon size={25} color={'#009624'} name='edit' />
                </TouchableOpacity>
                <TouchableOpacity onPress={() => setShowCriticalPopup(true)}>
                  <Icone size={25} color={'red'} name='trash' />
                </TouchableOpacity>
              </View>
            </View>
          )}
        />
      </View>
      <CriticalPopup
        show={showCriticalPopup}
        onSubmit={console.log('concluiu')}
        onDismiss={() => setShowCriticalPopup(false)}
        title='Tem certeza que deseja excluir o funcionário?'
        dangerText='Sim, quero excluir.'
        cancelText='Cancelar'
      />
      <Footer />
    </View>
  )
}
export default list
