import React, { useRef, useState } from 'react'
import {
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native'
import styles from '../../styles/index'
import Layout from '../layout'
import Footer from '../footer'
import { Formik } from 'formik'
import * as Yup from 'yup'
import HttpClient from '../../services/api'
import AsyncStorage from '@react-native-async-storage/async-storage'

function cadastroFuncionario({ navigation }) {
  const password = useRef(null)
  const email = useRef(null)
  const name = useRef(null)
  const apelido = useRef(null)

  const [submit, setSubmit] = useState(false)
  const [error, setError] = useState(false)
  const [errorMessage, setErrorMessage] = useState(false)

  const cadastroSchema = Yup.object().shape({
    email: Yup.string()
      .email('Digite um e-mail válido')
      .required('Preencha o campo de e-mail'),
    password: Yup.string().required('Preencha o campo de senha'),
    name: Yup.string().required('Nome é obrigatório'),
    apelido: Yup.string().required('Nome é obrigatório'),
  })

  const onSubmit = async (values) => {
    try {
      if (
        values.email.length === 0 ||
        values.name.length == 0 ||
        values.email.length == 0 ||
        values.password == 0
      ) {
        setError(true)
      } else {
        setError(false)
        setSubmit(true)
        const body = {
          email: values.email,
          name: values.name,
          password: values.password,
          surname: values.apelido,
        }
        const token = await AsyncStorage.getItem('@ComputacaoApp:token')
        console.log(token)
        const response = await HttpClient.post('/waiter', body, token)
        console.log(response.data)
        if (response && response.data && response.data.waiter) {
          setSubmit(false)
          navigation.push('Garcom')
        }
      }
    } catch (error) {
      setErrorMessage(true)
      console.log(error)
    }
  }

  return (
    <Formik
      initialValues={{
        email: '',
        password: '',
        apelido: '',
        email: '',
      }}
      validationSchema={cadastroSchema}
    >
      {({ values, handleChange, errors, touched, setFieldTouched }) => (
        <View style={styles.container}>
          <Layout />
          <ScrollView>
            <View style={styles.box}>
              <Text style={styles.heading}>Cadastrar Garçom</Text>
              <Text style={styles.label}> Nome Completo </Text>
              <TextInput
                style={styles.input}
                placeholder='Seu nome aqui'
                placeholderTextColor='#8E8E8E'
                onChangeText={handleChange('name')}
                ref={name}
                value={values.name}
                onBlur={() => setFieldTouched('name', true)}
              />
              {errors.name && touched.name && (
                <Text style={styles.error}>{errors.name}</Text>
              )}
              <Text style={styles.label}> Apelido </Text>
              <TextInput
                style={styles.input}
                placeholder='Seu apelido aqui'
                placeholderTextColor='#8E8E8E'
                onChangeText={handleChange('apelido')}
                ref={apelido}
                value={values.apelido}
                onBlur={() => setFieldTouched('apelido', true)}
              />
              {errors.apelido && touched.apelido && (
                <Text style={styles.error}>{errors.apelido}</Text>
              )}
              <Text style={styles.label}> E-mail </Text>
              <TextInput
                style={styles.input}
                placeholder='seuemail@exemplo.com.br'
                placeholderTextColor='#8E8E8E'
                onChangeText={handleChange('email')}
                ref={email}
                value={values.email}
                onBlur={() => setFieldTouched('email', true)}
              />
              {errors.email && touched.email && (
                <Text style={styles.error}>{errors.email}</Text>
              )}
              <Text style={styles.label}> Senha </Text>
              <TextInput
                style={styles.input}
                secureTextEntry={true}
                placeholder='*********'
                placeholderTextColor='#8E8E8E'
                onChangeText={handleChange('password')}
                ref={password}
                value={values.password}
                onBlur={() => setFieldTouched('password', true)}
              />
              {errors.password && touched.password && (
                <Text style={styles.error}>{errors.password}</Text>
              )}
              {error && (
                <Text style={styles.error}>
                  Preencha todos os campos para continuar!
                </Text>
              )}
              <TouchableOpacity
                style={styles.greenButton}
                onPress={() => onSubmit(values)}
                disabled={submit}
              >
                {!submit ? (
                  <Text style={styles.buttonText}>Cadastrar Garçom</Text>
                ) : (
                  <Text style={styles.buttonText}>Cadastrando..</Text>
                )}
              </TouchableOpacity>
            </View>
          </ScrollView>
          <Footer />
        </View>
      )}
    </Formik>
  )
}

export default cadastroFuncionario
