import React, { useRef } from 'react';
import { ScrollView, Text, TextInput, TouchableOpacity, View, useState } from 'react-native'
import styles from '../../styles/index';
import Layout from '../layout'
import Footer from '../footer'
import { Formik } from 'formik'
import * as Yup from 'yup';


function editFunc({ navigation }) {
    const password = useRef(null)
    const email = useRef(null)
    const name = useRef(null)
    const apelido = useRef(null)

    const cadastroSchema = Yup.object().shape({
        email: Yup.string()
            .email('Digite um e-mail válido')
            .required('Preencha o campo de e-mail'),
        password: Yup.string()
            .min(6, 'A senha deve ter no mínimo 6 caracteres')
            .required('Preencha o campo de senha'),
        name: Yup.string().required('Nome é obrigatório'),
        apelido: Yup.string().required('Nome é obrigatório'),
    });

    return (
        <Formik initialValues={{
            email: '',
            password: ''
        }}
            onSubmit={values => (console.log(values))}
            validationSchema={cadastroSchema}>
            {({ values, handleChange, handleSubmit, errors, touched, setFieldTouched }) =>
                <View style={styles.container}>
                    <Layout />
                    <ScrollView>
                        <View style={styles.box}>
                            <Text style={styles.heading}>Editar Garçom</Text>
                            <Text style={styles.label}> Nome Completo </Text>
                            <TextInput style={styles.input}
                                placeholder="Seu nome aqui"
                                placeholderTextColor="#8E8E8E"
                                onChangeText={handleChange('name')}
                                ref={name}
                                value={values.name}
                                onBlur={() => setFieldTouched('name', true)}
                            />
                            {errors.name && touched.name && <Text style={styles.error}>{errors.name}</Text>}
                            <Text style={styles.label}> Apelido </Text>
                            <TextInput style={styles.input}
                                placeholder="Seu apelido aqui"
                                placeholderTextColor="#8E8E8E"
                                onChangeText={handleChange('apelido')}
                                ref={apelido}
                                value={values.apelido}
                                onBlur={() => setFieldTouched('apelido', true)}
                            />
                            {errors.apelido && touched.apelido && <Text style={styles.error}>{errors.apelido}</Text>}
                            <Text style={styles.label}> E-mail </Text>
                            <TextInput style={styles.input}
                                placeholder="seuemail@exemplo.com.br"
                                placeholderTextColor="#8E8E8E"
                                onChangeText={handleChange('email')}
                                ref={email}
                                value={values.email}
                                onBlur={() => setFieldTouched('email', true)}
                            />
                            {errors.email && touched.email && <Text style={styles.error}>{errors.email}</Text>}
                            <Text style={styles.label}> Senha </Text>
                            <TextInput style={styles.input}
                                secureTextEntry={true}
                                placeholder="*********"
                                placeholderTextColor="#8E8E8E"
                                onChangeText={handleChange('password')}
                                ref={password}
                                value={values.password}
                                onBlur={() => setFieldTouched('password', true)}
                            />
                            {errors.password && touched.password && <Text style={styles.error}>{errors.password}</Text>}
                            <TouchableOpacity
                                style={styles.greenButton}
                                // disabled={errors.email && errors.password || !values.email && !values.password}
                                onPress={() => { handleSubmit; navigation.navigate('Garcom') }}
                            >
                                <Text
                                    style={styles.buttonText}
                                >Salvar alterações</Text>
                            </TouchableOpacity>
                        </View>
                    </ScrollView>
                    <Footer />
                </View >
            }
        </Formik >
    )
}

export default editFunc