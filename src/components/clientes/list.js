import React, { useState, useEffect } from 'react'
import {
  View,
  TouchableOpacity,
  Text,
  FlatList,
  ActivityIndicator,
} from 'react-native'
import styles from '../../styles'
import Footer from '../footer'
import Icon from 'react-native-vector-icons/MaterialIcons'
import HttpClient from '../../services/api'
import AsyncStorage from '@react-native-async-storage/async-storage'
function listClient({ navigation }) {
  const [data, setData] = useState([])
  const [loader, setLoader] = useState(false)

  useEffect(() => {
    async function loadCliente() {
      try {
        const token = await AsyncStorage.getItem('@ComputacaoApp:token')
        const response = await HttpClient.get('/customers', token)
        setData(response.data.customers.data)
        setLoader(true)
        console.log(response.data.customers.data)
      } catch (error) {
        console.log(error)
      }
    }
    loadCliente()
  }, [])
  return (
    <View style={styles.container}>
      <Text style={styles.headingClient}>Clientes</Text>
      {!loader && (
        <View style={[styles.loader, styles.horizontal]}>
          <ActivityIndicator size='large' color='#00ff00' />
        </View>
      )}
      <View style={styles.list}>
        <FlatList
          data={data}
          renderItem={({ item }) => (
            <View style={styles.listItem}>
              <Text style={styles.listInfo}>{item.name}</Text>
              <View
                style={{
                  justifyContent: 'space-between',
                  display: 'flex',
                  position: 'relative',
                  flexDirection: 'row',
                  flexWrap: 'wrap',
                  paddingEnd: 10,
                }}
              >
                <TouchableOpacity
                  onPress={() => navigation.navigate('EditarCliente')}
                  style={{ marginEnd: 10 }}
                >
                  <Icon size={25} color={'#009624'} name='edit' />
                </TouchableOpacity>
              </View>
            </View>
          )}
        />
      </View>
      <Footer />
    </View>
  )
}
export default listClient
