import React, { useRef } from 'react';
import { ScrollView, Text, TextInput, TouchableOpacity, View } from 'react-native'
import styles from '../../styles/index';
import Layout from '../layout'
import Footer from '../footer'
import { Formik } from 'formik'
import * as Yup from 'yup';


function editFunc({ navigation }) {
    const name = useRef(null)
    const apelido = useRef(null)

    const cadastroSchema = Yup.object().shape({
        name: Yup.string().required('Nome é obrigatório'),
        apelido: Yup.string().required('Nome é obrigatório'),
    });

    return (
        <Formik initialValues={{
            name: '',
            apelido: ''
        }}
            onSubmit={values => (console.log(values))}
            validationSchema={cadastroSchema}>
            {({ values, handleChange, handleSubmit, errors, touched, setFieldTouched }) =>
                <View style={styles.container}>
                    <Layout />
                    <ScrollView>
                        <View style={styles.box}>
                            <Text style={styles.heading}>Editar Cliente</Text>
                            <Text style={styles.label}> Nome Completo </Text>
                            <TextInput style={styles.input}
                                placeholder="Seu nome aqui"
                                placeholderTextColor="#8E8E8E"
                                onChangeText={handleChange('name')}
                                ref={name}
                                value={values.name}
                                onBlur={() => setFieldTouched('name', true)}
                            />
                            {errors.name && touched.name && <Text style={styles.error}>{errors.name}</Text>}
                            <Text style={styles.label}> Apelido </Text>
                            <TextInput style={styles.input}
                                placeholder="Seu apelido aqui"
                                placeholderTextColor="#8E8E8E"
                                onChangeText={handleChange('apelido')}
                                ref={apelido}
                                value={values.apelido}
                                onBlur={() => setFieldTouched('apelido', true)}
                            />
                            {errors.apelido && touched.apelido && <Text style={styles.error}>{errors.apelido}</Text>}
                            <TouchableOpacity
                                style={styles.greenButton}
                                // disabled={errors.email && errors.password || !values.email && !values.password}
                                onPress={() => { handleSubmit; navigation.navigate('Home') }}
                            >
                                <Text
                                    style={styles.buttonText}
                                >Salvar alterações</Text>
                            </TouchableOpacity>
                        </View>
                    </ScrollView>
                    <Footer />
                </View >
            }
        </Formik >
    )
}

export default editFunc