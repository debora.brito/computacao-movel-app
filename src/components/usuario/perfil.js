import React, { useState, useEffect } from 'react'
import { View, Text, ScrollView, TouchableOpacity } from 'react-native'
import styles from '../../styles/index'
import Footer from '../footer'
import Icon from 'react-native-vector-icons/Ionicons'
import AsyncStorage from '@react-native-async-storage/async-storage'

function perfil({ navigation }) {
  const [data, setData] = useState('')
  const [name, setName] = useState('')
  useEffect(() => {
    async function loadUser() {
      try {
        const user = await AsyncStorage.getItem('@ComputacaoApp:user')
        const obj = JSON.parse(user)
        setName(obj.name)
        var date1 = new Date(obj.created_at)
        var date2 = new Date()
        var timeDiff = Math.abs(
          new Date(date2).getTime() - new Date(date1).getTime()
        )
        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24))
        setData(diffDays)
      } catch (error) {
        console.log(error)
      }
    }

    loadUser()
  }, [])
  return (
    <View style={styles.container}>
      <Text style={styles.welcome}>Perfil</Text>
      <ScrollView>
        <View
          style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }}
        >
          <Icon name='person' size={150} color='#009624' />
        </View>
        <View>
          <Text style={styles.heading}>{name}</Text>
          <Text style={styles.subtitle}>Você está no time há {data} dias </Text>
        </View>
        <TouchableOpacity
          style={styles.greenButton}
          onPress={() => navigation.navigate('EditUser')}
        >
          <Text style={styles.buttonText}>Editar Perfil</Text>
        </TouchableOpacity>
      </ScrollView>
      <Footer />
    </View>
  )
}
export default perfil
