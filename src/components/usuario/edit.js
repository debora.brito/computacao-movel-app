import React, { useRef, useState, useEffect } from 'react'
import {
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native'
import styles from '../../styles/index'
import Layout from '../layout'
import Footer from '../footer'
import { Formik } from 'formik'
import * as Yup from 'yup'
import AsyncStorage from '@react-native-async-storage/async-storage'
import HttpClient from '../../services/api'

function editUser() {
  const [error, setError] = useState(false)
  const [errorMessage, setErrorMessage] = useState(false)
  const [submit, setSubmit] = useState(false)
  const [id, setId] = useState('')
  const [sucess, setSucess] = useState(false)

  const name = useRef(null)
  const email = useRef(null)

  const loginSchema = Yup.object().shape({
    email: Yup.string()
      .email('Digite um e-mail válido')
      .required('Preencha o campo de e-mail'),
    name: Yup.string().required('Nome é obrigatório'),
  })

  const [initialValues, setInitialValues] = useState({
    email: '',
    name: '',
  })
  useEffect(() => {
    async function loadUser() {
      try {
        const user = await AsyncStorage.getItem('@ComputacaoApp:user')
        const obj = JSON.parse(user)
        setInitialValues({
          ...initialValues,
          email: obj.email,
          name: obj.name,
        })
        setId(obj.id)
      } catch (error) {
        console.log(error)
      }
    }
    loadUser()
  }, [])

  const onSubmit = async (values) => {
    try {
      console.log(values)
      if (values.email.length === 0 || values.name.length == 0) {
        setError(true)
      } else {
        setError(false)
        setErrorMessage(false)
        setSubmit(true)
        const body = {
          email: values.email,
          name: values.name,
        }
        const token = await AsyncStorage.getItem('@ComputacaoApp:token')
        const response = await HttpClient.put(`/user/${id}`, body, token)
        console.log(response.data)
        if (response && response.data && response.data.user) {
          await AsyncStorage.setItem(
            '@ComputacaoApp:user',
            JSON.stringify(response.data.user)
          )
          setSucess(true)
          setSubmit(false)
        }
      }
    } catch (error) {
      setErrorMessage(true)
    }
  }

  return (
    <Formik
      enableReinitialize={true}
      validationSchema={loginSchema}
      initialValues={initialValues}
    >
      {({
        values,
        handleChange,
        errors,
        touched,
        setFieldTouched,
      }) => (
        <View style={styles.container}>
          <Layout />
          {console.log(initialValues)}
          <ScrollView>
            <View style={styles.box}>
              <Text style={styles.heading}>Editar Perfil</Text>
              <Text style={styles.label}> Nome Completo </Text>
              <TextInput
                style={styles.input}
                defaultValue={initialValues.name}
                ref={name}
                onChangeText={handleChange('name')}
                onBlur={() => setFieldTouched('name', true)}
              />
              {errors.name && touched.name && (
                <Text style={styles.error}>{errors.name}</Text>
              )}
              <Text style={styles.label}> E-mail </Text>
              <TextInput
                style={styles.input}
                onChangeText={handleChange('email')}
                defaultValue={initialValues.email}
                ref={email}
                onChangeText={handleChange('email')}
                onBlur={() => setFieldTouched('email', true)}
              />
              {errors.email && touched.email && (
                <Text style={styles.error}>{errors.email}</Text>
              )}
              {sucess && (
                <Text style={{ color: 'green' }}>Salvo com sucesso</Text>
              )}
              {error && (
                <Text style={styles.error}>
                  Preencha todos os campos para continuar!
                </Text>
              )}
              <TouchableOpacity
                style={styles.greenButton}
                onPress={() => onSubmit(values)}
                disabled={submit}
              >
                {!submit ? (
                  <Text style={styles.buttonText}>Salvar alterações</Text>
                ) : (
                  <Text style={styles.buttonText}>Salvando...</Text>
                )}
              </TouchableOpacity>
            </View>
          </ScrollView>
          <Footer />
        </View>
      )}
    </Formik>
  )
}

export default editUser
