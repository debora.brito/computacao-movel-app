import React, { Component } from 'react'
import styles from '../styles/footer/index';
import { ScrollView, Text, View } from 'react-native'

function footer() {
    return (
        <View style={styles.FooterCopyright}>
            <Text style={{ color: 'white' }} >
                Pena Verde {'| '}
                {new Date().getFullYear()}
            </Text>
        </View >

    )
}

export default footer