import React from 'react';
import PropTypes from 'prop-types';
import styles from '../styles'
import { TouchableOpacity, View, Text } from 'react-native';

function CriticalPopup({
    show, title, legend, dangerText, cancelText, onSubmit, onDismiss,
}) {
    if (!show) return null;
    return (
        <View style={styles.popUpContainer}>
            <View style={styles.popUpWrapper}>
                <Text style={styles.popUpTitle}>{title}</Text>
                <Text style={styles.popUpLegend}>{legend}</Text>
                <TouchableOpacity
                    style={styles.greenButton}
                    onPress={onSubmit}
                >
                    <Text style={styles.buttonText} onClick={onSubmit}>{dangerText}</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    style={styles.redButton}
                    onPress={onDismiss}
                >
                    <Text style={styles.buttonText} >{cancelText}</Text>
                </TouchableOpacity>
            </View>
        </View>
    );
}

CriticalPopup.propTypes = {
    title: PropTypes.string,
    legend: PropTypes.string,
    dangerText: PropTypes.string,
    cancelText: PropTypes.string,
    onSubmit: PropTypes.func.isRequired,
    onDismiss: PropTypes.func.isRequired,
};

CriticalPopup.defaultProps = {
    title: 'Concluir operação?',
    legend: 'Esta ação é irreversível',
    dangerText: 'Sim, continuar',
    cancelText: 'Cancelar',
};

export default CriticalPopup;