import React, { useRef, useState } from 'react';
import { ScrollView, Text, TextInput, TouchableOpacity, View } from 'react-native'
import styles from '../../styles/index';
import Layout from '../layout'
import Footer from '../footer'
import { Formik } from 'formik'
import * as Yup from 'yup';
import { TextInputMask} from 'react-native-masked-text';

function editProduto({ navigation }) {
    const name = useRef(null)
    const price = useRef(null)

    const [inputMoeda, setinputMoeda] = useState('0')

    const cadastroSchema = Yup.object().shape({
        name: Yup.string().required('Nome é obrigatório'),
        price: Yup.number().typeError('Deve ser um número').required('O preço é obrigatório')
    });

    return (
        <Formik initialValues={{
            name: '',
            price: ''
        }}
            onSubmit={values => (console.log(values))}
            validationSchema={cadastroSchema}>
            {({ values, handleChange, handleSubmit, errors, touched, setFieldTouched }) =>
                <View style={styles.container}>
                    <Layout />
                    <ScrollView>
                        <View style={styles.box}>
                            <Text style={styles.heading}>Editar Produto</Text>
                            <Text style={styles.label}> Nome do Produto </Text>
                            <TextInput style={styles.input}
                                placeholder="Nome do produto aqui"
                                placeholderTextColor="#8E8E8E"
                                onChangeText={handleChange('name')}
                                ref={name}
                                value={values.name}
                                onBlur={() => setFieldTouched('name', true)}
                            />
                            {errors.name && touched.name && <Text style={styles.error}>{errors.name}</Text>}
                            <Text style={styles.label}> Preço </Text>
                            <TextInputMask
                                style={styles.input}
                                type={'money'}
                                value={inputMoeda}
                                maxLength={18}
                                onChangeText={value => {
                                    setinputMoeda(value)
                                    value = value.replace('R$', '')
                                    value = value.replace('.', '')
                                    value = value.replace(',', '.')
                                }}
                                ref={price}
                                onBlur={() => setFieldTouched('price', true)}
                            />
                            {errors.price && touched.price && <Text style={styles.error}>{errors.price}</Text>}
                            <TouchableOpacity
                                style={styles.greenButton}
                                // disabled={errors.email && errors.password || !values.email && !values.password}
                                onPress={() => { handleSubmit; navigation.navigate('ListarProduto') }}
                            >
                                <Text
                                    style={styles.buttonText}
                                >Salvar alterações</Text>
                            </TouchableOpacity>
                        </View>
                    </ScrollView>
                    <Footer />
                </View >
            }
        </Formik >
    )
}

export default editProduto