import React, { useRef, useState } from 'react'
import {
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native'
import styles from '../../styles/index'
import Layout from '../layout'
import Footer from '../footer'
import { Formik } from 'formik'
import * as Yup from 'yup'
import { TextInputMask } from 'react-native-masked-text'
import HttpClient from '../../services/api'
import AsyncStorage from '@react-native-async-storage/async-storage'

function cadastroProduto({ navigation }) {
  const name = useRef(null)
  const price = useRef(null)

  const [inputMoeda, setinputMoeda] = useState('0')
  const [error, setError] = useState(false)
  const [submit, setSubmit] = useState(false)

  const cadastroSchema = Yup.object().shape({
    name: Yup.string().required('Nome é obrigatório'),
    price: Yup.string().required('O preço é obrigatório'),
  })

  const onSubmit = async (values) => {
    try {
      if (values.name.length === 0 || values.price.length == 0) {
        setError(true)
      } else {
        setError(false)
        setSubmit(true)
        const body = {
          name: values.name,
          price: String(values.price)
            .replace('R$', '')
            .replace(/\./g, '')
            .replace(',', '.'),
        }
        const token = await AsyncStorage.getItem('@ComputacaoApp:token')
        console.log(token)
        const response = await HttpClient.post('/products', body, token)
        console.log(response.data)
        if (response && response.data && response.data.product) {
          setSubmit(false)
          navigation.push('ListarProduto')
        }
      }
    } catch (error) {
      setErrorMessage(true)
      console.log(error)
    }
  }
  return (
    <Formik
      initialValues={{
        name: '',
        price: '',
      }}
      validationSchema={cadastroSchema}
    >
      {({ values, handleChange, errors, touched, setFieldTouched }) => (
        <View style={styles.container}>
          <Layout />
          <ScrollView>
            <View style={styles.box}>
              <Text style={styles.heading}>Cadastrar Produto</Text>
              <Text style={styles.label}> Nome do Produto </Text>
              <TextInput
                style={styles.input}
                placeholder='Nome do produto aqui'
                placeholderTextColor='#8E8E8E'
                onChangeText={handleChange('name')}
                ref={name}
                value={values.name}
                onBlur={() => setFieldTouched('name', true)}
              />
              {errors.name && touched.name && (
                <Text style={styles.error}>{errors.name}</Text>
              )}
              <Text style={styles.label}> Preço </Text>
              <TextInputMask
                style={styles.input}
                type={'money'}
                value={inputMoeda}
                onChangeText={handleChange('price')}
                ref={price}
                onBlur={() => setFieldTouched('price', true)}
              />
              {errors.price && touched.price && (
                <Text style={styles.error}>{errors.price}</Text>
              )}
              {error && (
                <Text style={styles.error}>
                  Preencha todos os campos para continuar!
                </Text>
              )}
              <TouchableOpacity
                style={styles.greenButton}
                onPress={() => onSubmit(values)}
                disabled={submit}
              >
                {!submit ? (
                  <Text style={styles.buttonText}>Cadastrar Produto</Text>
                ) : (
                  <Text style={styles.buttonText}>Cadastrando..</Text>
                )}
              </TouchableOpacity>
            </View>
          </ScrollView>
          <Footer />
        </View>
      )}
    </Formik>
  )
}

export default cadastroProduto
