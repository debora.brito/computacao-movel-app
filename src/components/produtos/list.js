import React, { useState, useEffect } from 'react'
import {
  View,
  TouchableOpacity,
  Text,
  FlatList,
  ActivityIndicator,
} from 'react-native'
import styles from '../../styles'
import Footer from '../footer'
import Icon from 'react-native-vector-icons/MaterialIcons'
import Icone from 'react-native-vector-icons/FontAwesome'
import CriticalPopup from '../popup'
import HttpClient from '../../services/api'
import AsyncStorage from '@react-native-async-storage/async-storage'

function listProducts({ navigation }) {
  const [showCriticalPopup, setShowCriticalPopup] = useState(false)
  const [data, setData] = useState([])
  const [loader, setLoader] = useState(false)
  useEffect(() => {
    async function loadProduct() {
      try {
        const token = await AsyncStorage.getItem('@ComputacaoApp:token')
        const response = await HttpClient.get('/products', token)
        setData(response.data.products.data)
        setLoader(true)
        console.log(response.data.products.data)
      } catch (error) {
        console.log(error)
      }
    }
    loadProduct()
  }, [])
  return (
    <View style={styles.container}>
      <TouchableOpacity
        style={styles.greenButton}
        onPress={() => navigation.navigate('CadastrarProduto')}
      >
        <Text style={styles.buttonText}>Cadastrar Novo Produto</Text>
      </TouchableOpacity>
      <Text style={styles.headingClient}>Produtos</Text>
      {!loader && (
        <View style={[styles.loader, styles.horizontal]}>
          <ActivityIndicator size='large' color='#00ff00' />
        </View>
      )}
      <View style={styles.list}>
        <FlatList
          data={data}
          renderItem={({ item }) => (
            <View style={styles.listItemProduct}>
              <View
                style={{
                  display: 'flex',
                  flexDirection: 'column',
                  alignItems: 'flex-start',
                  padding: 20,
                  justifyContent: 'space-evenly',
                }}
              >
                <Text style={styles.listInfo}>{item.name}</Text>
                <Text style={styles.info}>R$ {item.price}</Text>
              </View>
              <View
                style={{
                  justifyContent: 'space-between',
                  display: 'flex',
                  position: 'relative',
                  flexDirection: 'row',
                  flexWrap: 'wrap',
                  paddingEnd: 20,
                }}
              >
                <TouchableOpacity
                  onPress={() => navigation.navigate('EditarProduto')}
                  style={{ marginEnd: 10 }}
                >
                  <Icon size={25} color={'#009624'} name='edit' />
                </TouchableOpacity>
                <TouchableOpacity onPress={() => setShowCriticalPopup(true)}>
                  <Icone size={25} color={'red'} name='trash' />
                </TouchableOpacity>
              </View>
            </View>
          )}
        />
      </View>
      <CriticalPopup
        show={showCriticalPopup}
        onSubmit={console.log('concluiu')}
        onDismiss={() => setShowCriticalPopup(false)}
        title='Tem certeza que deseja excluir o produto?'
        dangerText='Sim, quero excluir.'
        cancelText='Cancelar'
      />
      <Footer />
    </View>
  )
}
export default listProducts
