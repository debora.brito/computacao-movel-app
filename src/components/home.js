import React, { useEffect, useState } from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import styles from '../styles/index'
import Icon from 'react-native-vector-icons/Ionicons'
import Icone from 'react-native-vector-icons/MaterialIcons'
import Ic from 'react-native-vector-icons/Fontisto'
import MaterialIcon from 'react-native-vector-icons/MaterialCommunityIcons'
import FontIcon from 'react-native-vector-icons/FontAwesome5'
import Footer from './footer'
import AsyncStorage from '@react-native-async-storage/async-storage'

function home({ navigation }) {
  const [data, setData] = useState('')

  useEffect(() => {
    async function loadUser() {
      try {
        const user = await AsyncStorage.getItem('@ComputacaoApp:user')
        const obj = JSON.parse(user)
        setData(obj)
      } catch (error) {
        console.log(error)
      }
    }

    loadUser()
  }, [])


  return (
    <View style={styles.container}>
      <Text style={styles.welcome}>Olá, {data.name} </Text>
      <View style={styles.dashboardMenu}>
        <TouchableOpacity onPress={() => navigation.navigate('Perfil')}>
          <View style={styles.dashBoardItem}>
            <Icon name='person' size={50} color='#009624' />
            <Text style={styles.dashBoardText}>Perfil</Text>
          </View>
        </TouchableOpacity>

        <TouchableOpacity onPress={() => navigation.navigate('CadastrarVenda')}>
          <View style={styles.dashBoardItem}>
            <Icone name='attach-money' size={50} color='#009624' />
            <Text style={styles.dashBoardText}>Vendas</Text>
          </View>
        </TouchableOpacity>

        <TouchableOpacity onPress={() => navigation.navigate('Garcom')}>
          <View style={styles.dashBoardItem}>
            <Ic name='persons' size={50} color='#009624' />
            <Text style={styles.dashBoardText}>Funcionários</Text>
          </View>
        </TouchableOpacity>

        <TouchableOpacity onPress={() => navigation.navigate('ListarProduto')}>
          <View style={styles.dashBoardItem}>
            <MaterialIcon name='food-fork-drink' size={50} color='#009624' />
            <Text style={styles.dashBoardText}>Produtos</Text>
          </View>
        </TouchableOpacity>

        <TouchableOpacity onPress={() => navigation.navigate('ListarCliente')}>
          <View style={styles.dashBoardItem}>
            <FontIcon name='users' size={50} color='#009624' />
            <Text style={styles.dashBoardText}>Clientes</Text>
          </View>
        </TouchableOpacity>
      </View>
      <Footer />
    </View>
  )
}

export default home
