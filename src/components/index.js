import React, { Component } from 'react';
import { createAppContainer } from "react-navigation"
import { createStackNavigator } from "react-navigation-stack"
import Login from './Login'
import Home from './home'
import Perfil from './usuario/perfil'
import EditUser from './usuario/edit'
import Garcom from './funcionarios/list'
import CadastrarFunc from './funcionarios/cadastro'
import EditFunc from './funcionarios/edit'
import ListarProduto from './produtos/list'
import EditarProduto from './produtos/edit'
import CadastrarProduto from './produtos/cadastro'
import ListarCliente from './clientes/list'
import EditarCliente from './clientes/edit'
import CadastrarVenda from './vendas/index'

const MainNav = createStackNavigator(
    {
        Login: {
            screen: Login,
            navigationOptions: {
                headerTintColor: '#00C853',
                headerTitle: 'Pena Verde',
            }
        },
        Home: {
            screen: Home,
            navigationOptions: {
                headerTintColor: '#00C853',
                headerTitle: 'Pena Verde',
            }
        },
        Perfil: {
            screen: Perfil,
            navigationOptions: {
                headerTintColor: '#00C853',
                headerTitle: 'Pena Verde',
            }
        },
        EditUser: {
            screen: EditUser,
            navigationOptions: {
                headerTintColor: '#00C853',
                headerTitle: 'Pena Verde',
            }
        },
        Garcom: {
            screen: Garcom,
            navigationOptions: {
                headerTintColor: '#00C853',
                headerTitle: 'Pena Verde',
            }
        },
        CadastrarFunc: {
            screen: CadastrarFunc,
            navigationOptions: {
                headerTintColor: '#00C853',
                headerTitle: 'Pena Verde',
            }
        },
        EditFunc: {
            screen: EditFunc,
            navigationOptions: {
                headerTintColor: '#00C853',
                headerTitle: 'Pena Verde',
            }
        },
        ListarProduto: {
            screen: ListarProduto,
            navigationOptions: {
                headerTintColor: '#00C853',
                headerTitle: 'Pena Verde',
            }
        },
        EditarProduto: {
            screen: EditarProduto,
            navigationOptions: {
                headerTintColor: '#00C853',
                headerTitle: 'Pena Verde',
            }
        },
        CadastrarProduto: {
            screen: CadastrarProduto,
            navigationOptions: {
                headerTintColor: '#00C853',
                headerTitle: 'Pena Verde',
            }
        },
        ListarCliente: {
            screen: ListarCliente,
            navigationOptions: {
                headerTintColor: '#00C853',
                headerTitle: 'Pena Verde',
            }
        },
        EditarCliente: {
            screen: EditarCliente,
            navigationOptions: {
                headerTintColor: '#00C853',
                headerTitle: 'Pena Verde',
            }
        },
        CadastrarVenda: {
            screen: CadastrarVenda,
            navigationOptions: {
                headerTintColor: '#00C853',
                headerTitle: 'Pena Verde',
            }
        },
    }
)

export default createAppContainer(MainNav)