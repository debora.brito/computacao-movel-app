import React, { Component } from 'react'
import { View, StatusBar } from 'react-native'

function layout() {
    return (
        <View >
            <StatusBar
                barStyle="dark-content"
                hidden={false}
                backgroundColor="green"
                translucent={false}
                networkActivityIndicatorVisible={true}
            />
        </View>
    )
}

export default layout

