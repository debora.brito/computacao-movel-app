import React, { useRef, useState } from 'react'
import {
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native'
import AsyncStorage from '@react-native-async-storage/async-storage'
import styles from '../styles/index'
import Layout from './layout'
import Footer from './footer'
import { ErrorMessage, Formik } from 'formik'
import * as Yup from 'yup'
import HttpClient from '../services/api'

function App({ navigation, values }) {
  const password = useRef(null)
  const email = useRef(null)
  const [error, setError] = useState(false)
  const [errorMessage, setErrorMessage] = useState(null)
  const [submit, setSubmit] = useState(false)

  const loginSchema = Yup.object().shape({
    email: Yup.string()
      .email('Digite um e-mail válido')
      .required('Preencha o campo de e-mail'),
    password: Yup.string().required('Preencha o campo de senha'),
  })

  const initialValues = {
    email: '',
    password: '',
  }
  const onSubmit = async (values) => {
    try {
      if (values.email.length === 0 || values.password.length === 0) {
        setError(true)
      } else {
        setError(false)
        setErrorMessage(false)
        setSubmit(true)
        const body = {
          email: values.email,
          password: values.password,
        }
        const response = await HttpClient.post('/login', body)
        if (response && response.data && response.data.user) {
          await AsyncStorage.setItem(
            '@ComputacaoApp:token',
            `Bearer ${response.data.token}`
          )
          await AsyncStorage.setItem(
            '@ComputacaoApp:user',
            JSON.stringify(response.data.user)
          )
          navigation.navigate('Home')
        }
      }
    } catch (error) {
      if (error.message.includes('401')) {
        setErrorMessage('Email ou Senha inválidos')
      } else if (error.message.includes('500')) {
        setErrorMessage('Sem Conexão')
      }
      setSubmit(false)
    }
  }

  return (
    <Formik initialValues={initialValues} validationSchema={loginSchema}>
      {({ values, handleChange, errors, touched, setFieldTouched }) => (
        <View style={styles.container}>
          <Layout />
          <ScrollView>
            <View style={styles.box}>
              <Text style={styles.heading}>Acessar Conta</Text>
              <Text style={styles.label}> E-mail </Text>
              <TextInput
                style={styles.input}
                placeholder='seuemail@exemplo.com.br'
                placeholderTextColor='#8E8E8E'
                onChangeText={handleChange('email')}
                ref={email}
                value={values.email}
                onBlur={() => setFieldTouched('email', true)}
              />
              {errors.email && touched.email && (
                <Text style={styles.error}>{errors.email}</Text>
              )}
              <Text style={styles.label}> Senha </Text>
              <TextInput
                style={styles.input}
                secureTextEntry={true}
                placeholder='*********'
                placeholderTextColor='#8E8E8E'
                onChangeText={handleChange('password')}
                ref={password}
                value={values.password}
                onBlur={() => setFieldTouched('password', true)}
              />
              {errors.password && touched.password && (
                <Text style={styles.error}>{errors.password}</Text>
              )}
              {error && (
                <Text style={styles.error}>
                  Preencha usuário e senha para continuar!
                </Text>
              )}
              {errorMessage && <Text style={styles.error}>{errorMessage}</Text>}
              <TouchableOpacity
                style={styles.greenButton}
                onPress={() => onSubmit(values)}
                disabled={submit}
              >
                {!submit ? (
                  <Text style={styles.buttonText}>Entrar na conta</Text>
                ) : (
                  <Text style={styles.buttonText}>Entrando...</Text>
                )}
              </TouchableOpacity>
            </View>
          </ScrollView>
          <Footer />
        </View>
      )}
    </Formik>
  )
}

export default App
