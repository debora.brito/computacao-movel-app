import React, { useState, useEffect } from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import { useFormikContext } from 'formik'
import styles from '../../../styles'
import AsyncStorage from '@react-native-async-storage/async-storage'

function Preview() {
  const { values, setFieldValue } = useFormikContext()
  const [tableHead] = useState(['Produto', 'Quantidade'])
  const [data, setData] = useState('')

  useEffect(() => {
    async function loadUser() {
      try {
        const user = await AsyncStorage.getItem('@ComputacaoApp:user')
        const obj = JSON.parse(user)
        setData(obj)
      } catch (error) {
        console.log(error)
      }
    }

    loadUser()
  }, [])

  const [submit, setSubmit] = useState(false)
  const [sucess, setSucess] = useState(false)
  const [errorMessage, setErrorMessage] = useState(false)

  const onSubmit = async () => {
    try {
      const body = {
        name: values.name,
        surname: values.apelido,
        products: values.product,
        tip: parseFloat(values.product.price),
        idCliente: values.idCliente,
      }
      console.log(body)
      const token = await AsyncStorage.getItem('@ComputacaoApp:token')
      const response = await HttpClient.post('/sale', body, token)
      console.log(response.data)
      if (response && response.data && response.data.sale) {
        setSubmit(false)
        setSUcess(true)
      }
    } catch (error) {
      setErrorMessage(true)
      console.log(error)
    }
  }

  return (
    <View>
      <Text style={styles.heading}>Revise os dados</Text>
      <Text style={styles.headingReview}>Cliente</Text>
      <Text>{values.name}</Text>
      <Text style={styles.headingReview}>Garçom</Text>
      <Text>{data.name}</Text>
      <Text style={styles.headingReview}>Produtos</Text>
      {values.product.map((prod) => (
        <View
          style={{
            display: 'flex',
            position: 'relative',
            flexDirection: 'row',
            flexWrap: 'wrap',
            justifyContent: 'flex-start',
          }}
        >
          <Text style={{ fontWeight: 'bold' }}>Produto:</Text>
          <Text style={{ paddingRight: 10 }}> {prod.product}</Text>
          <Text style={{ fontWeight: 'bold' }}>Quantidade:</Text>
          <Text>{prod.quantidade}</Text>
          <Text style={{ paddingLeft: 10, fontWeight: 'bold' }}>Preço:</Text>
          <Text>{prod.quantidade}</Text>
        </View>
      ))}
      <View
        style={{
          display: 'flex',
          justifyContent: 'space-between',
          flexDirection: 'row',
        }}
      >
        <TouchableOpacity
          style={styles.outlinedButton}
          onPress={() => setFieldValue('next', 'produto')}
        >
          <Text style={styles.outlinedText}>Voltar</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.greenButton} onPress={() => onSubmit()}>
          <Text style={styles.buttonText}>Concluir</Text>
        </TouchableOpacity>
      </View>
    </View>
  )
}
export default Preview
