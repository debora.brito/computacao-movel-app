import RadioButton from '../../radio'
import { useFormikContext } from 'formik'
import React, { useState } from 'react'
import styles from '../../../styles/index'
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native'
import ModalFilterPicker from 'react-native-modal-filter-picker'
import Icon from 'react-native-vector-icons/AntDesign'
import { useRef, useEffect } from 'react'
import HttpClient from '../../../services/api'
import AsyncStorage from '@react-native-async-storage/async-storage'

function addCliente() {
  const name = useRef(null)
  const apelido = useRef(null)
  const [isLiked, setIsLiked] = useState([
    { id: 1, value: true, name: 'Sim', selected: null },
    { id: 2, value: false, name: 'Não', selected: null },
  ])

  const [visible, setVisible] = useState(false)
  const [picked, setPicked] = useState('Cliente')
  const [data, setData] = useState([])
  const [loader, setLoader] = useState(false)

  const onRadioBtnClick = (item) => {
    let updatedState = isLiked.map((isLikedItem) =>
      isLikedItem.id === item.id
        ? { ...isLikedItem, selected: true }
        : { ...isLikedItem, selected: false }
    )
    setIsLiked(updatedState)
  }
  const value = isLiked.reduce((item) => item.selected)

  const {
    values,
    setFieldValue,
    handleChange,
    setFieldTouched,
    initialValues,
  } = useFormikContext()

  useEffect(() => {
    async function loadCliente() {
      try {
        const token = await AsyncStorage.getItem('@ComputacaoApp:token')
        const response = await HttpClient.get('/customers', token)
        setData(response.data.customers.data)
        setLoader(true)
      } catch (error) {
        console.log(error)
      }
    }
    loadCliente()
  }, [])

  const options = data.map((item) => {
    return {
      key: item.id,
      label: item.name,
      searchKey: item.surname,
    }
  })

  const onShow = () => {
    setVisible(true)
  }

  const onSelect = (picked) => {
    setPicked(picked.label)
    setVisible(false)
    initialValues.name = picked.label
    initialValues.idCliente = picked.key
    initialValues.apelido = picked.searchKey
  }

  const onCancel = () => {
    setVisible(false)
  }

  return (
    <>
      <View>
        <Text style={styles.heading}>Adicionar Cliente</Text>
        <Text style={styles.radioText}>O cliente já possui cadastro?</Text>
        {isLiked.map((item) => (
          <RadioButton
            onPress={() => {
              onRadioBtnClick(item)
            }}
            selected={item.selected}
            key={item.id}
          >
            {item.name}
          </RadioButton>
        ))}
        {value === false && (
          <View>
            <Text style={styles.label}> Nome do Cliente* </Text>
            <TextInput
              style={styles.input}
              placeholder='Nome do cliente aqui'
              placeholderTextColor='#8E8E8E'
              ref={name}
              value={values.name}
              onChangeText={handleChange('name')}
              onBlur={() => setFieldTouched('name', true)}
            />
            <Text style={styles.label}> Apelido do Cliente </Text>
            <TextInput
              style={styles.input}
              placeholder='Apelido do cliente aqui'
              placeholderTextColor='#8E8E8E'
              ref={apelido}
              value={values.apelido}
              onChangeText={handleChange('apelido')}
              onBlur={() => setFieldTouched('apelido', true)}
            />
          </View>
        )}
        {value === true && (
          <View style={styles.selectedContainer}>
            <Text style={styles.label}>Selecione um cliente</Text>
            {!loader ? (
              <View style={[styles.loader, styles.horizontal]}>
                <ActivityIndicator size='large' color='#00ff00' />
              </View>
            ) : (
              <TouchableOpacity style={styles.buttonContainer} onPress={onShow}>
                <Text style={{ color: '#8E8E8E' }}>{picked}</Text>
                <Icon name='down' size={21} color={'#009624'} />
              </TouchableOpacity>
            )}

            <ModalFilterPicker
              visible={visible}
              onSelect={onSelect}
              onCancel={onCancel}
              options={options}
              ref={name}
              placeholderText={'Pesquise pelo cliente'}
              cancelButtonText={'Cancelar'}
              noResultsText={'Cliente não encontrado'}
              cancelButtonStyle={styles.redButton}
              cancelButtonTextStyle={styles.buttonText}
              overlayStyle={styles.overlayStyle}
              listContainerStyle={styles.listContainerStyle}
              optionTextStyle={styles.optionTextStyle}
              filterTextInputContainerStyle={
                styles.filterTextInputContainerStyle
              }
              filterTextInputStyle={styles.filterTextInputStyle}
            />
          </View>
        )}
        <TouchableOpacity
          style={styles.greenButton}
          onPress={() => setFieldValue('next', 'produto')}
        >
          <Text style={styles.buttonText}>Continuar</Text>
        </TouchableOpacity>
      </View>
    </>
  )
}

export default addCliente
