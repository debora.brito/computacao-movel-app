import { Formik } from 'formik'
import React from 'react'
import { View, Text } from 'react-native'
import Cliente from '../Cliente/index'
import Produto from '../Produtos/index'
import Visualizar from '../Visualizar/index'

function VendaForm({ initialValues }) {
  return (
    <Formik initialValues={initialValues}>
      {({ values }) => (
        <View>
          {values.next === 'cliente' && <Cliente />}
          {values.next === 'produto' && <Produto />}
          {values.next === 'revisao' && <Visualizar />}
        </View>
      )}
    </Formik>
  )
}

export default VendaForm
