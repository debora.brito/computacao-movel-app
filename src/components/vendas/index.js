import React from 'react'
import styles from '../../styles/index'
import { View, Text, ScrollView } from 'react-native'
import Footer from '../footer'
import VendaForm from './Form/index'
import HttpClient from '../../services/api'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { useState } from 'react'
import { useFormikContext } from 'formik'

function Form() {
  const initialValues = {
    next: 'cliente',
    name: '',
    apelido: '',
    products: [],
    tip: '',
    idCliente: '',
  }

  return (
    <View style={styles.container}>
      <Text style={styles.headingClient}>Venda</Text>
      <ScrollView>
        <View style={styles.box}>
          <VendaForm initialValues={initialValues} />
        </View>
      </ScrollView>
      <Footer />
    </View>
  )
}

export default Form
