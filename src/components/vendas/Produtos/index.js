import { useFormikContext } from 'formik'
import React, { useState, useRef, useEffect } from 'react'
import styles from '../../../styles/index'
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native'
import ModalFilterPicker from 'react-native-modal-filter-picker'
import Icon from 'react-native-vector-icons/AntDesign'
import { Table, TableWrapper, Row, Cell } from 'react-native-table-component'
import { FAB } from 'react-native-paper'
import Icone from 'react-native-vector-icons/FontAwesome'
import HttpClient from '../../../services/api'
import AsyncStorage from '@react-native-async-storage/async-storage'

function addProduto() {
  const [visible, setVisible] = useState(false)
  const [picked, setPicked] = useState('Produto')
  const [data, setData] = useState([])
  const [loader, setLoader] = useState(false)
  const [product, setProduct] = useState([])
  const [tableHead] = useState(['Produto', 'Quantidade', 'Ação'])
  const quantidade = useRef(null)

  const alertIndex = (index) => {
    alert(`This is row ${index}`)
  }

  const element = (data, index) => (
    <View style={styles.elementContainer}>
      <Text style={styles.textElement}>{data.product}</Text>
      <Text style={styles.elements}>{data.quantidade}</Text>
      <TouchableOpacity onPress={() => alertIndex(index)}>
        <Icone style={styles.icon} size={25} color={'red'} name='trash' />
      </TouchableOpacity>
    </View>
  )

  const {
    values,
    setFieldValue,
    validateForm,
    handleChange,
    setFieldTouched,
    initialValues,
  } = useFormikContext()

  useEffect(() => {
    async function loadCliente() {
      try {
        const token = await AsyncStorage.getItem('@ComputacaoApp:token')
        const response = await HttpClient.get('/products', token)
        setProduct(response.data.products.data)
        setLoader(true)
        console.log(response.data.products.data)
      } catch (error) {
        console.log(error)
      }
    }
    loadCliente()
  }, [])

  const options = product.map((item) => {
    return {
      key: item.id,
      label: item.name,
      searchKey: item.price,
    }
  })

  const onShow = () => {
    setVisible(true)
  }

  const onSelect = (pick) => {
    setPicked(pick.label)
    setVisible(false)
  }

  const onCancel = () => {
    setVisible(false)
  }

  const addProduct = () => {
    if (picked !== 'Produto') {
      const copy = Array.from(data)
      setData(copy)
      initialValues.products = copy
    }
  }

  return (
    <View style={{ flex: 1 }}>
      <Text style={styles.heading}>Adicionar Produto</Text>
      <View style={styles.selectedContainer}>
        <Text style={styles.label}>Selecione um produto</Text>
        {!loader ? (
          <View style={[styles.loader, styles.horizontal]}>
            <ActivityIndicator size='large' color='#00ff00' />
          </View>
        ) : (
          <View>
            <TouchableOpacity style={styles.buttonContainer} onPress={onShow}>
              <Text style={{ color: '#8E8E8E' }}>{picked}</Text>
              <Icon name='down' size={21} color={'#009624'} />
            </TouchableOpacity>
            <Text style={styles.label}>Quantidade</Text>
            <TextInput
              style={styles.input}
              placeholder='Quantidade desse produto aqui'
              placeholderTextColor='#8E8E8E'
              ref={quantidade}
              value={values.quantidade}
              onChangeText={handleChange('quantidade')}
              onBlur={() => setFieldTouched('quantidade', true)}
            />
            <FAB
              style={styles.buttonPopUp}
              small
              icon='plus'
              onPress={addProduct}
            />
          </View>
        )}
        <ModalFilterPicker
          visible={visible}
          onSelect={onSelect}
          onCancel={onCancel}
          options={options}
          placeholderText={'Pesquise pelo produto'}
          cancelButtonText={'Cancelar'}
          noResultsText={'Produto não encontrado'}
          cancelButtonStyle={styles.redButton}
          cancelButtonTextStyle={styles.buttonText}
          overlayStyle={styles.overlayStyle}
          listContainerStyle={styles.listContainerStyle}
          optionTextStyle={styles.optionTextStyle}
          filterTextInputContainerStyle={styles.filterTextInputContainerStyle}
          filterTextInputStyle={styles.filterTextInputStyle}
        />
      </View>
      <View style={styles.con}>
        <Table borderStyle={{ borderColor: 'transparent' }}>
          <Row data={tableHead} style={styles.head} textStyle={styles.text} />
          {data.map((item, index) => (
            <TableWrapper key={index} style={styles.row}>
              <Cell key={index} data={element(item, item.quantidade)} />
            </TableWrapper>
          ))}
        </Table>
      </View>
      <View
        style={{
          display: 'flex',
          justifyContent: 'space-between',
          flexDirection: 'row',
        }}
      >
        <TouchableOpacity
          style={styles.outlinedButton}
          onPress={() => setFieldValue('next', 'cliente')}
        >
          <Text style={styles.outlinedText}>Voltar</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.greenButton}
          onPress={() => {
            setFieldValue('next', 'revisao')
            setFieldValue('product', data)
          }}
        >
          <Text style={styles.buttonText}>Continuar</Text>
        </TouchableOpacity>
      </View>
    </View>
  )
}

export default addProduto
